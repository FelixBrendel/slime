* mallocs

|------------------------------------------+------------------------|
| location                                 | always freed           |
|------------------------------------------+------------------------|
| [[file:3rd\ftb\arraylist.hpp::11]]:          | yes                    |
| [[file:.\3rd\ftb\bucket_allocator.hpp::17]]: | yes                    |
| [[file:.\3rd\ftb\bucket_allocator.hpp::44]]: | yes                    |
| [[file:.\3rd\ftb\bucket_allocator.hpp::46]]: | yes                    |
| [[file:.\src\io.cpp::49]]:                   | yes                    |
| [[file:.\src\io.cpp::164]]:                  | yes                    |
| [[file:.\src\io.cpp::209]]:                  | yes                    |
| [[file:.\src\io.cpp::285]]:                  | yes                    |
| [[file:.\src\memory.cpp::158]]:              | yes in free_everything |
| [[file:.\src\platform.cpp::3]]:              | yes                    |
| [[file:.\src\platform.cpp::27]]:             | yes                    |
| [[file:.\src\platform.cpp::47]]:             | yes                    |
| [[file:.\src\platform.cpp::81]]:             | yes                    |
|------------------------------------------+------------------------|

* news

|----------------------------------+-------------------------------------------------------------------------------------|
| location                         | always deleted                                                                      |
|----------------------------------+-------------------------------------------------------------------------------------|
| [[file:.\src\eval.cpp::262]]:        | ::new (&(result->positional.symbols)) Array_List<Lisp_Object*>;                     |
| [[file:.\src\eval.cpp::263]]:        | ::new (&(result->keyword.keywords))   Array_List<Lisp_Object*>;                     |
| [[file:.\src\eval.cpp:264]]:         | ::new (&(result->keyword.values))     Array_List<Lisp_Object*>;                     |
| [[file:.\src\io.cpp:284]]:           | // allocate a new block of memory size char (1 byte) instead of wide char (2 bytes) |
| [[file:.\src\io.cpp:306]]:           | wchar_t* wc = new wchar_t[cSize];                                                   |
| [[file:.\src\memory.cpp:336]]:       | // node->value.lambdaWrapper = new Lambda_Wrapper(function);                        |
| [[file:.\src\memory.cpp:383]]:       | // inject a new array list;                                                         |
| [[file:.\src\parse.cpp:195]]:        | // better for keeping track of the encountered new lines and                        |
| [[file:.\src\parse.cpp:196]]:        | // characters since last new line so we can update the parser                       |
| [[file:.\src\parse.cpp:208]]:        | /* new col = (count chars since last \n) + 1 */                                     |
|----------------------------------+-------------------------------------------------------------------------------------|
