/**
   Usage of the create_error_macros:
*/
#define __create_error(keyword, ...)            \
    create_error(                               \
        __FUNCTION__, __FILE__, __LINE__,       \
        Memory::get_keyword(keyword),           \
        __VA_ARGS__)

#define create_out_of_memory_error(...)                 \
    __create_error("out-of-memory", __VA_ARGS__)

#define create_generic_error(...)               \
    __create_error("generic", __VA_ARGS__)

#define create_not_yet_implemented_error()                                              \
    __create_error("not-yet-implemented", "This feature has not yet been implemented.")

#define create_parsing_error(...)                       \
    __create_error("parsing-error", __VA_ARGS__)

#define create_symbol_undefined_error(...)              \
    __create_error("symbol-undefined", __VA_ARGS__)

#define create_type_missmatch_error(expected, actual, exp)                            \
    __create_error("type-missmatch",                                                  \
                   "Type missmatch: expected %{l_o_t}, got %{l_o_t} in %{l_o_r}",       \
                   expected, actual, exp)

#ifdef _DEBUG

#define assert_type(_node, _type)                                           \
    do {                                                                    \
        if (_node->type != _type) {                                         \
            create_type_missmatch_error(                                    \
                _type,                                                      \
                _node->type,                                                \
                _node);                                                     \
        }                                                                   \
    } while(0)

#define assert_list_length(_node, _len) assert("List length assertion", list_length(_node) == _len)
#define assert_vector_length(_node, _len) assert("Vector length assertion", _node->value.vector.length == _len)

#define assert(message, condition)                                              \
    do {                                                                        \
        if (!(condition)) {                                                     \
            create_generic_error("Assertion-error: %s\n"                        \
                                 "      condition: %s\n"                        \
                                 "             in: %s:%d",                      \
                                 message, #condition, __FILE__, __LINE__);      \
        }                                                                       \
    } while(0)

#else
#  define assert_arguments_length(expected, actual)               do {} while (0)
#  define assert_arguments_length_less_equal(expected, actual)    do {} while (0)
#  define assert_arguments_length_greater_equal(expected, actual) do {} while (0)
#  define assert_type(_node, _type)                               do {} while (0)
#  define assert_list_length(_node, _len)                         do {} while (0)
#  define assert(message, condition)                              do {} while (0)
#endif
