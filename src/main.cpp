#include "libslime.cpp"

using namespace Slime;

s32 main(s32 argc, char* argv[]) {
#ifdef SLIME_WINDOWS
    // enable colored terminal output for windows
    HANDLE hOut  = GetStdHandle(STD_OUTPUT_HANDLE);
    DWORD dwMode = 0;
    GetConsoleMode(hOut, &dwMode);
    dwMode |= ENABLE_VIRTUAL_TERMINAL_PROCESSING;
    SetConsoleMode(hOut, dwMode);
#endif

    Memory::init();
    Memory::load_pre();
    Memory::push_user_environment();
    defer {
        if_debug {
            Memory::free_everything();
        }
    };

    if (argc > 1) {
        if (string_equal(argv[1], "--run-tests")) {
            s32 res = run_all_tests();
            return res ? 0 : 1;
        } else if (string_equal(argv[1], "--generate-docs-file")) {
            if (Globals::error) return 1;
            built_in_load(Memory::create_string("generate-docs-file.slime"));
        } else {
            interprete_file(argv[1]);
        }
    } else {
        interprete_stdin();
        return 0;
    }

    if (Globals::error) return 1;
}
