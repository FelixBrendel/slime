namespace Slime {
    proc define_symbol(Lisp_Object* symbol, Lisp_Object* value) -> void {
        profile_with_comment(symbol->value.symbol.data);
        define_symbol(symbol, value, get_current_environment());
    }

    proc define_symbol(Lisp_Object* symbol, Lisp_Object* value, Environment* env) -> void {
        profile_with_comment(symbol->value.symbol.data);
        env->hm.set_object((void*)symbol, value);
    }

    inline proc lookup_symbol_in_this_envt(Lisp_Object* sym, Environment* env) -> Lisp_Object* {
        return (Lisp_Object*)env->hm.get_object((void*)sym);
    }

    proc environment_binds_symbol(Lisp_Object* sym, Environment* env) -> bool {
        return lookup_symbol_in_this_envt(sym, env) != nullptr;
    }

    proc find_binding_environment(Lisp_Object* sym, Environment* env) -> Environment* {
        if (environment_binds_symbol(sym, env))
            return env;
        for (auto it : env->parents) {
            if (Environment* ret = find_binding_environment(sym, it))
                return ret;
        }
        return nullptr;
    }

    proc try_lookup_symbol(Lisp_Object* node, Environment* env) -> Lisp_Object* {
        // first check current environment
        static Lisp_Object* nil_sym = Memory::get_symbol("nil");
        static Lisp_Object* t_sym   = Memory::get_symbol("t");

        Lisp_Object* result;
        result = lookup_symbol_in_this_envt(node, env);
        if (result)
            return result;

        // NOTE(Felix): We have to traverse the parents in reverse
        // order of adding them. The thing we imported last, should
        // overshadow the things imported earlier
        for (s32 i = (s32)env->parents.next_index-1; i >= 0; --i) {
            result = try_lookup_symbol(node, env->parents.data[i]);

            if (result)
                return result;
        }

        if (node == nil_sym) {
            return Memory::nil;
        } else if (node == t_sym) {
            return Memory::t;
        }

        return nullptr;
    }

    inline proc push_environment(Environment* env) -> void {
        Globals::Current_Execution.envi_stack.append(env);
    }

    inline proc pop_environment() -> void {
        --Globals::Current_Execution.envi_stack.next_index;
    }

    inline proc get_root_environment() -> Environment* {
        return Globals::Current_Execution.envi_stack.data[0];
    }

    inline proc get_current_environment() -> Environment* {
        return Globals::Current_Execution.envi_stack.data[
            Globals::Current_Execution.envi_stack.next_index-1];
    }

    proc lookup_symbol(Lisp_Object* node, Environment* env) -> Lisp_Object* {
        profile_with_comment(node->value.symbol.data);
        assert("env was null", env);
        // print(node);
        assert_type(node, Lisp_Object_Type::Symbol);

        Lisp_Object* result = try_lookup_symbol(node, env);

        if (result)
            return result;

        String identifier = node->value.symbol;
        print("%{env}\n", env);
        create_symbol_undefined_error("The symbol '%s' is not defined.", identifier.data);
        return nullptr;
    }


    proc print_environment(FILE* file, Environment* env) -> int {
        int written;

        const proc print_environment_indent = [&](const auto & self, Environment* env, u32 indent) -> void {
            proc print_indent = [&]() -> int{
                for (u32 i = 0; i < indent; ++i) {
                    print_to_file(file, " ");
                }
                return indent;
            };

            if(env == get_root_environment()) {
                written += print_indent();
                written += print_to_file(file, "[built-ins]-Environment (0x%p)\n", env);
                return;
            }

            for_hash_map (env->hm) {
                written += print_indent();
                written += print_to_file(file, "-> %{str} :: %{l_o} (%{ptr})\n",
                                         ((Lisp_Object*)key)->value.symbol.data, value, value);
            }
            for (u32 i = 0; i < env->parents.next_index; ++i) {
                written += print_indent();
                written += print_to_file(file,"parent (%{ptr}):", env->parents.data[i]);
                self(self, env->parents.data[i], indent+4);
            }
        };

        written = print_to_file(file, "\n=== Environment === %{ptr}\n", env);
        print_environment_indent(print_environment_indent, env, 0);
        return written;
    }

}
