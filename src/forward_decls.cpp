namespace Slime {
    void add_to_load_path(const path_char*);
    bool lisp_object_equal(Lisp_Object*,Lisp_Object*);
    Lisp_Object* built_in_load(String);
    Lisp_Object* built_in_import(String);
    void delete_error();
    void create_error(const char* c_func_name, const char* c_file_name, u32 c_file_line, Lisp_Object* type, const char* format, ...);
    void create_error(const char* c_func_name, const char* c_file_name, u32 c_file_line, Lisp_Object* type, String message);
    void create_error(Lisp_Object* type, const char* message, const char* c_file_name, u32 c_file_line);
    Lisp_Object* eval_arguments(Lisp_Object*);
    Lisp_Object* eval_expr(Lisp_Object*);
    bool is_truthy (Lisp_Object*);
    u32 list_length(Lisp_Object*);
    void* load_built_ins_into_environment();
    void create_arguments_from_lambda_list_and_inject(Lisp_Object* formal_arguments, Lisp_Object* function);

    Lisp_Object* lookup_symbol(Lisp_Object* symbol, Environment*);
    void define_symbol(Lisp_Object* symbol, Lisp_Object* value);
    void define_symbol(Lisp_Object* symbol, Lisp_Object* value, Environment* env);
    // char* lisp_object_to_string(Lisp_Object* node, bool print_repr = true);

    // void print_lisp_object(Lisp_Object* node, bool print_repr = false, FILE* file = stdout);
    int print_lisp_object(FILE*, Lisp_Object*);
    int print_lisp_object_repr(FILE*, Lisp_Object*);
    int print_lisp_object_type(FILE*, Lisp_Object_Type);
    int print_environment(FILE*, Environment*);

    inline char* duplicate_c_string(const char* str);

    char* char_to_wchar(const wchar_t* c);
    wchar_t* char_to_wchar(const char* c);
    path_char* char_to_path_char(const char* c);
    char* path_char_to_char(const path_char* c);

    inline char* duplicate_c_string(const char* str);

    bool run_all_tests();

    inline Environment* get_root_environment();
    inline Environment* get_current_environment();
    inline void push_environment(Environment*);
    inline void pop_environment();

    void visualize_lisp_machine();
    void generate_docs(String path);
    void log_error();

    namespace Memory {
        Environment* create_built_ins_environment();
        Lisp_Object* create_lisp_object_cfunction(bool is_special);
        void init();
        void load_pre();
        char* get_c_str(String);
        void free_everything();
        String create_string(const char*);
        String create_string_no_alloc(const char*);
        Lisp_Object* get_symbol(String identifier);
        Lisp_Object* get_symbol(const char*);
        Lisp_Object* get_keyword(String identifier);
        Lisp_Object* get_keyword(const char*);
        Lisp_Object* create_lisp_object(f64);
        Lisp_Object* create_lisp_object(void*);
        Lisp_Object* create_lisp_object(const char*);
        Lisp_Object* create_lisp_object_continuation();
        Lisp_Object* create_lisp_object_vector(Lisp_Object*);
        Lisp_Object* create_lisp_object_vector(Lisp_Object*, Lisp_Object*);
        Lisp_Object* create_lisp_object_vector(Lisp_Object*, Lisp_Object*, Lisp_Object*);
        Lisp_Object* create_lisp_object_vector(u32, Lisp_Object*);
        inline Lisp_Object* create_list(Lisp_Object*);
        inline Lisp_Object* create_list(Lisp_Object*,Lisp_Object*);
        inline Lisp_Object* create_list(Lisp_Object*,Lisp_Object*,Lisp_Object*);
        inline Lisp_Object* create_list(Lisp_Object*,Lisp_Object*,Lisp_Object*,Lisp_Object*);
        inline Lisp_Object* create_list(Lisp_Object*,Lisp_Object*,Lisp_Object*,Lisp_Object*,Lisp_Object*);
        inline Lisp_Object* create_list(Lisp_Object*,Lisp_Object*,Lisp_Object*,Lisp_Object*,Lisp_Object*,Lisp_Object*);
    }

    namespace Parser {
        // extern Environment* environment_for_macros;

        extern String standard_in;
        extern String parser_file;
        extern u32 parser_line;
        extern u32 parser_col;

        Lisp_Object* parse_expression(char* text, u32* index_in_text);
        Lisp_Object* parse_single_expression(const char* text);
        Lisp_Object* parse_single_expression(char*    text);
        Lisp_Object* parse_single_expression(wchar_t* text);
    }

    namespace Globals {
        extern bool debug_log;
        extern char* bin_path;
        extern Log_Level log_level;
        extern Array_List<path_char*> load_path;
        // namespace Current_Execution {
            // extern Array_List<Lisp_Object*> call_stack;
            // extern Array_List<Environment*> envi_stack;
        // }
        extern Continuation Current_Execution;
        extern Error* error;
        extern bool breaking_on_errors;
    }
}
