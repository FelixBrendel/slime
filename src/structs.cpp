namespace Slime {
    struct Lisp_Object;
    struct String;
    struct Environment;

    enum struct Thread_Type : u8 {
        Main,
        GarbageCollection
    };

    enum struct Lisp_Object_Type : u8 {
        Nil,
        T,
        Symbol,
        Keyword,
        Number,
        String,
        Pair,
        Vector,
        Continuation,
        Pointer,
        HashMap,
        Function,
        Invalid_Garbage_Collected,
        Invalid_Under_Construction
    };

    enum struct NasAction : u8 {
        And_Then_Action,
        Macro_Write_Back,
        Eval,
        Step,
        TM,
        Pop,
        If,
        Define_Var,
        Pop_Environment
    };

    enum struct Lisp_Function_Type : u8 {
        Lambda, // normal evaluation order
        Macro   // args are not evaluated, a new programm is returned
                // that will be executed again
    };

    enum struct C_Function_Type : u8 {
        cFunction,  // normal evaluation order
        cSpecial,   // args are not evaluated, but result is returned
                    // as you would expect
        cMacro      // No return value, but the current_execution is
                    // modified
    };

    enum struct Log_Level : u8 {
        None,
        Critical,
        Warning,
        Info,
        Debug,
    };

    struct Continuation {
        Array_List<Lisp_Object*>            cs; // call stack
        Array_List<Lisp_Object*>           pcs; // program counter stack
        Array_List<int>                    ams; // apply marker stack
        Array_List<Array_List<NasAction>> nass; // next action stack stack
        Array_List<Lambda<void()>>         ats; // and then stack
        Array_List<Lisp_Object*>           mes; // macro expansion stack
        Array_List<Environment*>           envi_stack;
    };

    struct String {
        u32   length;
        char* data;
    };

    struct Source_Code_Location {
        String file;
        u32 line;
        u32 column;
    };

    struct Pair {
        Lisp_Object* first;
        Lisp_Object* rest;
    };

    struct Vector {
        u32 length;
        Lisp_Object* data;
    };

    struct Positional_Arguments {
        Array_List<Lisp_Object*> symbols;
    };

    struct Keyword_Arguments {
        // Array of Pointers to Lisp_Object<Keyword>
        Array_List<Lisp_Object*> keywords;
        // NOTE(Felix): values[i] will be nullptr if no defalut value was
        // declared for key identifiers[i]
        Array_List<Lisp_Object*> values;
    };

    struct Arguments {
        Positional_Arguments positional;
        Keyword_Arguments    keyword;
        // NOTE(Felix): rest_argument will be nullptr if no rest argument
        // is declared otherwise its a symbol
        Lisp_Object*         rest;
    };

    struct Environment {
        Array_List<Environment*> parents;
        Hash_Map<void*, Lisp_Object*> hm;
    };

    struct Function {
        Arguments     args;
        Environment*  parent_environment;
        bool          is_c;
        union {
            Lisp_Function_Type lisp_function_type;
            C_Function_Type    c_function_type;
        } type;
        union {
            Lisp_Object* lisp_body;
            Lisp_Object* (*c_body)();
            void (*c_macro_body)();
        } body;
    };

// #pragma pack(1)
    struct Lisp_Object {
        Lisp_Object_Type type;
        union value {
            String              symbol; // used for symbols and keywords
            f64                 number;
            String              string;
            Pair                pair;
            Vector              vector;
            Function*           function;
            void*               pointer;
            Continuation*       continuation;
            Hash_Map<Lisp_Object*, Lisp_Object*>* hashMap;
        } value;
    };
// #pragma options align=reset
    struct Error {
        Lisp_Object* position;
        // type has to be a keyword
        Lisp_Object* type;
        char*        message;
    };
}
