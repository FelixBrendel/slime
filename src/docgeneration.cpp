namespace Slime {
    proc generate_docs(String path) -> void {
        print("Generating Docs...");
        FILE *f = fopen(Memory::get_c_str(path), "w");
        if (!f) {
            create_generic_error("The file for writing the documentation (%s) "
                                 "could not be opened for writing.", Memory::get_c_str(path));
            return;
        }
        defer {
            fclose(f);
        };

        Array_List<Environment*> visited;
        visited.alloc();
        defer {
            visited.dealloc();
        };

        const auto print_this_env = [&](const auto& rec, Environment* env, char* prefix) -> void {
            bool we_already_printed = false;
            // TODO(Felix): Make a generic array_list_contains function
            for(auto it : visited) {
                if (it == env) {
                    we_already_printed = true;
                    break;
                }
            }
            if (!we_already_printed) {
                // print("Working on env::::%{env}",env);
                // printf("\n--------------------------------\n");
                visited.append(env);

                push_environment(env);
                defer {
                    pop_environment();
                };

                for_hash_map(env->hm) {
                    try_void fprintf(f,
                                     "#+latex: \\vspace{0.5cm}\n"
                                     "#+latex: \\hrule\n"
                                     // "#+latex: \\hspace{0.5cm}\n"
                                     "#+html: <hr/>\n"
                                     "* =%s%s= \n"
                                     "  :PROPERTIES:\n"
                                     "  :UNNUMBERED: t\n"
                                     "  :END:"
                                     ,prefix, Memory::get_c_str(((Lisp_Object*)key)->value.symbol));
                    /*
                     * sourcecodeLocation
                     */
                    // TODO(Felix): Enable again when we have SCL again:

                    // if (value->sourceCodeLocation) {
                    //     try_void fprintf(f, "\n - defined in ::  =%s:%d:%d=",
                    //                      Memory::get_c_str(value->sourceCodeLocation->file),
                    //                      value->sourceCodeLocation->line,
                    //                      value->sourceCodeLocation->column);
                    // }
                    /*
                     * type
                     */
                    Lisp_Object_Type type = value->type;
                    Lisp_Object* LOtype;
                    Lisp_Object* type_expr = Memory::create_list(Memory::get_symbol("type"), value);
                    try_void LOtype = eval_expr(type_expr);

                    fprintf(f, "\n*type*\\newline\\indent\n");
                    print_to_file(f, "=%{l_o_r}=\\newline\\noindent", LOtype);

                    /*
                     * if printable value -> print it
                     */
                    switch (type) {
                    case(Lisp_Object_Type::Nil):
                    case(Lisp_Object_Type::T):
                    case(Lisp_Object_Type::Number):
                    case(Lisp_Object_Type::String):
                    case(Lisp_Object_Type::Pair):
                    case(Lisp_Object_Type::Symbol):
                    case(Lisp_Object_Type::Keyword): {
                        print_to_file(f, "\n*value*\\newline\\indent =%{l_o_r}=\\newline\\noindent", value);
                    } break;
                    default: break;
                    }
                    /*
                     * if function then print arguments
                     */
                    if (type == Lisp_Object_Type::Function)
                    {
                        Arguments* args = &value->value.function->args;
                        fprintf(f, "\n*signature*\n");

                        fprintf(f,
                                "#+BEGIN:\n"
                                "#+BEGIN_SRC slime\n"
                                "(%s%s", prefix, Memory::get_c_str(((Lisp_Object*)key)->value.symbol));

                        // if some args at all
                        if (args->positional.symbols.next_index != 0 ||
                            args->keyword.values.next_index    != 0 ||
                            args->rest)
                        {
                            if (args->positional.symbols.next_index != 0) {
                                for (auto lo: args->positional.symbols) {
                                    fprintf(f, " %s", lo->value.symbol.data);
                                }
                            }
                            if (args->keyword.values.next_index != 0) {
                                for (u32 i = 0; i < args->keyword.values.next_index; ++i) {
                                    // if has default value
                                    if (args->keyword.values.data[i]) {
                                        print_to_file(f, " (:%s %{l_o})",
                                                      args->keyword.keywords.data[i]->value.symbol.data,
                                                      args->keyword.values.data[i]);
                                    } else {
                                        fprintf(f, " :%s", args->keyword.keywords.data[i]->value.symbol.data);
                                    }
                                }
                            }
                            if (args->rest) {
                                fprintf(f, " . %s", Memory::get_c_str(args->rest->value.symbol));
                            }
                        }
                        fprintf(f,
                                ")\n"
                                "#+END_SRC\n"
                                "#+END:\n");
                    }
                    fprintf(f, "\n\\noindent\n*docu*\n");
                    char* docs = Globals::docs.get_object(value);
                    fprintf(f, "\n  #+BEGIN:\n%s\n  #+END:\n",
                            docs ? docs : "none");
                }
            }

            for (u32 i = 0; i < env->parents.next_index; ++i) {
                try_void rec(rec, env->parents.data[i], prefix);
            }
        };

        print_this_env(print_this_env, get_current_environment(), (char*)"");
        print("Done!\n");
    }
}
