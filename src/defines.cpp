#define proc auto

#ifdef _DEBUG
# define SLIME_DEBUG
# define if_debug if constexpr (true)
#else
# define if_debug if constexpr (false)
#endif


#if defined(_MSC_VER) || defined(_WIN32) || defined(WIN32) || defined(_WIN64)
# define SLIME_WINDOWS
# define debug_break() if_debug __debugbreak()
# define if_windows if constexpr (true)
# define if_linux   if constexpr (false)
#else
# define debug_break() if_debug raise(SIGTRAP)
# define if_windows if constexpr (false)
# define if_linux   if constexpr (true)
#endif

#define console_normal "\x1B[0m"
#define console_red    "\x1B[31m"
#define console_green  "\x1B[32m"
#define console_cyan   "\x1B[36m"
