namespace Slime {
    proc string_equal(const char input[], const char check[]) -> bool {
        if (input == check) return true;

        for(u32 i = 0; input[i] == check[i]; i++) {
            if (input[i] == '\0')
                return true;
        }

        return false;
    }

    proc string_equal(String str, const char check[]) -> bool {
        return string_equal(Memory::get_c_str(str), check);
    }

    proc string_equal(const char check[], String str) -> bool {
        return string_equal(Memory::get_c_str(str), check);
    }

    proc string_equal(String str1, String str2) -> bool {
        return string_equal(Memory::get_c_str(str1), Memory::get_c_str(str2));
    }

    proc get_nibble(char c) -> char {
        if (c >= 'A' && c <= 'F')
            return (c - 'A') + 10;
        else if (c >= 'a' && c <= 'f')
            return (c - 'a') + 10;
        return (c - '0');
    }

    proc escape_string(char* in) -> char* {
        // TODO(Felix): add more escape sequences
        u32 i = 0, count = 0;
        while (in[i] != '\0') {
            switch (in[i]) {
            case '\\':
            case '\n':
            case '\t':
                ++count;
            default: break;
            }
            ++i;
        }

        char* ret = (char*)malloc((i+count+1)*sizeof(char));

        // copy in
        i = 0;
        u32 j = 0;
        while (in[i] != '\0') {
            switch (in[i]) {
            case '\\': ret[j++] = '\\'; ret[j++] = '\\'; break;
            case '\n': ret[j++] = '\\'; ret[j++] = 'n';  break;
            case '\t': ret[j++] = '\\'; ret[j++] = 't';  break;
            default: ret[j++] = in[i];
            }
            ++i;
        }
        ret[j++] = '\0';
        return ret;
    }

    proc unescape_string(char* in) -> s32 {
        if (!in) return 0;

        char *out = in, *p = in;
        const char *int_err = nullptr;

        while (*p && !int_err) {
            if (*p != '\\') {
                /* normal case */
                *out++ = *p++;
            } else {
                /* escape sequence */
                switch (*++p) {
                case '0': *out++ = '\a'; ++p; break;
                case 'a': *out++ = '\a'; ++p; break;
                case 'b': *out++ = '\b'; ++p; break;
                case 'f': *out++ = '\f'; ++p; break;
                case 'n': *out++ = '\n'; ++p; break;
                case 'r': *out++ = '\r'; ++p; break;
                case 't': *out++ = '\t'; ++p; break;
                case 'v': *out++ = '\v'; ++p; break;
                case '"':
                case '\'':
                case '\\':
                    *out++ = *p++;
                case '?':
                    break;
                case 'x':
                case 'X':
                    if (!isxdigit(p[1]) || !isxdigit(p[2])) {
                        create_parsing_error(
                            "The string '%s' at %s:%d:%d could not be unescaped. "
                            "(Invalid character on hexadecimal escape at char %d)",
                            in, Parser::parser_file, Parser::parser_line, Parser::parser_col,
                            (p+1)-in);
                    } else {
                        *out++ = (char)(get_nibble(p[1]) * 0x10 + get_nibble(p[2]));
                        p += 3;
                    }
                    break;
                default:
                    create_parsing_error(
                        "The string '%s' at %s:%d:%d could not be unescaped. "
                        "(Unexpected '\\' with no escape sequence at char %d)",
                        in, Parser::parser_file, Parser::parser_line, Parser::parser_col,
                        (p+1)-in);
                }
            }
        }

        /* Set the end of string. */
        *out = '\0';
        return (s32)(out - in);
    }

    proc read_entire_file(path_char* filename) -> char* {
        profile_this();
        char *fileContent = nullptr;
#if defined(UNICODE) && defined(SLIME_WINDOWS)
        FILE *fp = _wfopen(filename, L"r");
#else
        FILE *fp = fopen(filename, "r");
#endif
        if (fp) {
            /* Go to the end of the file. */
            if (fseek(fp, 0L, SEEK_END) == 0) {
                /* Get the size of the file. */
                long bufsize = ftell(fp) + 1;
                if (bufsize == 0) {
                    fputs("Empty file", stderr);
                    goto closeFile;
                }

                /* Go back to the start of the file. */
                if (fseek(fp, 0L, SEEK_SET) != 0) {
                    fputs("Error reading file", stderr);
                    goto closeFile;
                }

                /* Allocate our buffer to that size. */
                fileContent = (char*)calloc(bufsize, sizeof(char));

                /* Read the entire file into memory. */
                size_t newLen = fread(fileContent, sizeof(char), bufsize, fp);

                fileContent[newLen] = '\0';
                if (ferror(fp) != 0) {
                    fputs("Error reading file", stderr);
                }
            }
          closeFile:
            fclose(fp);
        }

        return fileContent;
        /* Don't forget to call free() later! */
    }

    proc read_expression() -> char* {
        char* line = (char*)malloc(100);

        if(line == nullptr)
            return nullptr;

        char* linep = line;
        size_t lenmax = 100, len = lenmax;
        s32 c;

        s32 nesting = 0;

        while (true) {
            c = fgetc(stdin);
            if(c == EOF)
                break;

            if(--len == 0) {
                len = lenmax;
                char * linen = (char*)realloc(linep, lenmax *= 2);

                if(linen == nullptr) {
                    free(linep);
                    return nullptr;
                }
                line = linen + (line - linep);
                linep = linen;
            }

            *line = (char)c;
            if(*line == '(')
                ++nesting;
            else if(*line == ')')
                --nesting;
            else if(*line == '\n')
                if (nesting == 0)
                    break;
            line++;
        }
        (*line)--; // we dont want the \n actually
        *line = '\0';

        return linep;
    }

    proc read_line() -> char* {
        char* line = (char*)malloc(100), * linep = line;
        size_t lenmax = 100, len = lenmax;
        s32 c;

        s32 nesting = 0;

        if(line == nullptr)
            return nullptr;

        for(;;) {
            c = fgetc(stdin);
            if(c == EOF)
                break;

            if(--len == 0) {
                len = lenmax;
                char* linen = (char*)realloc(linep, lenmax *= 2);

                if(linen == nullptr) {
                    free(linep);
                    return nullptr;
                }
                line = linen + (line - linep);
                linep = linen;
            }

            *line = (char)c;
            if(*line == '(')
                ++nesting;
            else if(*line == ')')
                --nesting;
            else if(*line == '\n')
                if (nesting == 0)
                    break;
            line++;
        }
        (*line)--; // we dont want the \n actually
        *line = '\0';

        return linep;
    }

    proc log_message(Log_Level type, const char* message) -> void {
        if (type > Globals::log_level)
            return;

        const char* prefix;
        switch (type) {
        case Log_Level::Critical: prefix = "CRITICAL"; break;
        case Log_Level::Warning:  prefix = "WARNING";  break;
        case Log_Level::Info:     prefix = "INFO";     break;
        case Log_Level::Debug:    prefix = "DEBUG";    break;
        default: return;
        }
        printf("%s: %s\n",prefix, message);
    }

    char* wchar_to_char(const wchar_t* pwchar) {
        // get the number of characters in the string.
        s32 currentCharIndex = 0;
        char currentChar = (char)pwchar[currentCharIndex];

        while (currentChar != '\0')
        {
            currentCharIndex++;
            currentChar = (char)pwchar[currentCharIndex];
        }

        const s32 charCount = currentCharIndex + 1;

        // allocate a new block of memory size char (1 byte) instead of wide char (2 bytes)
        char* filePathC = (char*)malloc(sizeof(char) * charCount);

        for (s32 i = 0; i < charCount; i++)
        {
            // convert to char (1 byte)
            char character = (char)pwchar[i];

            *filePathC = character;

            filePathC += sizeof(char);

        }
        filePathC += '\0';

        filePathC -= (sizeof(char) * charCount);

        return filePathC;
    }

    wchar_t* char_to_wchar(const char* c) {
        const size_t cSize = strlen(c)+1;
        wchar_t* wc = new wchar_t[cSize];
        mbstowcs (wc, c, cSize);

        return wc;
    }

    path_char* char_to_path_char(const char* c) {
#ifdef UNICODE
        return char_to_wchar(c);
#else
        return duplicate_c_string(c);
#endif
    }

    char* path_char_to_char(const path_char* c) {
#ifdef UNICODE
        return wchar_to_char(c);
#else
        return duplicate_c_string(c);
#endif
    }


    proc string_buider_to_string(Array_List<char*> string_builder) -> char* {
        size_t len = 1;
        for (auto str : string_builder) {
            len += strlen(str);
        }

        char* res = (char*)(malloc(sizeof(char) * len));
        res[0] = '\0';

        for (auto str : string_builder) {
            strcat(res, str);
        }

        return res;
    }

    proc print_lisp_object_optional(FILE* f, Lisp_Object* node, bool print_repr) -> int {
        int written = 0;

        if (!node)
            return print_to_file(f, "<nullptr>");

        switch (node->type) {
        case (Lisp_Object_Type::Nil):          return print_to_file(f, "()");
        case (Lisp_Object_Type::T):            return print_to_file(f, "t");
        case (Lisp_Object_Type::Continuation): return print_to_file(f, "[continuation]");
        case (Lisp_Object_Type::Pointer):      return print_to_file(f, "[pointer]");
        case (Lisp_Object_Type::Keyword):      return print_to_file(f, ":%s", Memory::get_c_str(node->value.symbol));
        case (Lisp_Object_Type::Symbol):       return print_to_file(f, ":%s", Memory::get_c_str(node->value.symbol));

        case (Lisp_Object_Type::Number): {
            if (abs(node->value.number - (s32)node->value.number) < 0.000001f)
                return print_to_file(f, "%d", (s32)node->value.number);
            else
                return print_to_file(f, "%f", node->value.number);
        }
        case (Lisp_Object_Type::HashMap): {
            for_hash_map (*(node->value.hashMap)) {
                written += fprintf(f, " ");
                written += print_lisp_object_optional(f, key, true);
                written += fprintf(f, " -> ");
                written += print_lisp_object_optional(f, (Lisp_Object*)value, true);
                written += fprintf(f, "\n");
            }
            return written;
        }
        case (Lisp_Object_Type::String): {
            if (print_repr) {
                char* escaped = escape_string(Memory::get_c_str(node->value.string));
                written = fprintf(f, "\"%s\"", escaped);
                free(escaped);
                return written;
            } else
                return print_to_file(f, "%s", Memory::get_c_str(node->value.string));
        } break;
        case (Lisp_Object_Type::Vector): {
            written += print_to_file(f, "[");
            if (node->value.vector.length > 0)
                written += print_lisp_object_optional(f, node->value.vector.data, print_repr);
            for (u32 i = 1; i < node->value.vector.length; ++i) {
                written += print_to_file(f, " ");
                written += print_lisp_object_optional(f, node->value.vector.data+i, print_repr);
            }
            written += print_to_file(f, "]");
            return written;
        } break;
        case (Lisp_Object_Type::Function): {
            if (Globals::user_types.key_exists(node)) {
                return print_to_file(f, "[%s]", ((Lisp_Object*)Globals::user_types.key_exists(node)) ->value.symbol.data);
            }

            if (node->value.function->is_c) {
                // NOTE(Felix): try to find the symbol it is bound to
                // in global env
                Lisp_Object* name = (Lisp_Object*)(get_root_environment()->hm.search_key_to_object(node));
                if (name) {
                    switch (node->value.function->type.c_function_type) {
                    case C_Function_Type::cFunction: return print_to_file(f, "[c-function %s]",name->value.symbol.data);
                    case C_Function_Type::cSpecial:  return print_to_file(f, "[c-special %s]", name->value.symbol.data);
                    case C_Function_Type::cMacro:    return print_to_file(f, "[c-macro %s]",   name->value.symbol.data);
                    default:                         return print_to_file(f, "[c-??]");
                    }
                } else {
                    switch (node->value.function->type.c_function_type) {
                    case C_Function_Type::cFunction: return print_to_file(f, "[c-function]");
                    case C_Function_Type::cSpecial:  return print_to_file(f, "[c-special]");
                    case C_Function_Type::cMacro:    return print_to_file(f, "[c-macro]");
                    default:                         return print_to_file(f, "[c-??]");
                    }
                }
            } else {
                switch (node->value.function->type.lisp_function_type) {
                case Lisp_Function_Type::Lambda: return print_to_file(f, "[lambda]");
                case Lisp_Function_Type::Macro:  return print_to_file(f, "[macro]");
                default:                         return print_to_file(f, "[??]");
                }
            }
        } break;
        case (Lisp_Object_Type::Pair): {
            Lisp_Object* head = node;

            // defer {
                // for (auto str : string_builder) {
                    // free(str);
                // }
            // };
            // first check if it is a quotation form, in that case we want
            // to print it prettier
            if (head->value.pair.first->type == Lisp_Object_Type::Symbol) {
                // String identifier = head->value.pair.first->value.symbol;


                auto symbol = head->value.pair.first;
                auto quote_sym            = Memory::get_symbol("quote");
                auto unquote_sym          = Memory::get_symbol("unquote");
                auto quasiquote_sym       = Memory::get_symbol("quasiquote");
                auto unquote_splicing_sym = Memory::get_symbol("unquote-splicing");
                // TODO(Felix): Maybe combine if and else? They look kinda the same
                if (symbol == quote_sym || symbol == unquote_sym || symbol == unquote_splicing_sym)
                {
                    if (symbol == quote_sym)
                        written += print_to_file(f, "\'");
                    else if (symbol == unquote_sym)
                        written += print_to_file(f, ",");
                    else if (symbol == unquote_splicing_sym)
                        written += print_to_file(f, ",@");

                    assert_type(head->value.pair.rest, Lisp_Object_Type::Pair);
                    assert("The list must end here.",
                           head->value.pair.rest->value.pair.rest == Memory::nil);

                    written += print_lisp_object_optional(f, head->value.pair.rest->value.pair.first, print_repr);
                    return written;
                } else if (symbol == quasiquote_sym) {
                    written += print_to_file(f, "`");
                    assert_type(head->value.pair.rest, Lisp_Object_Type::Pair);

                    written += print_lisp_object_optional(f, head->value.pair.rest->value.pair.first, print_repr);
                    return written;
                }
            }

            written += print_to_file(f, "(");

            // NOTE(Felix): We could do a while true here, however in case
            // we want to print a broken list (for logging the error) we
            // should do more checks.
            while (head) {
                written += print_lisp_object_optional(f, head->value.pair.first, print_repr);
                head = head->value.pair.rest;
                if (!head)                                break;
                if (head->type != Lisp_Object_Type::Pair) break;
                written += print_to_file(f, " ");
            }

            if (head && head != Memory::nil) {
                written += print_to_file(f, " . ");
                written += print_lisp_object_optional(f, head, print_repr);
            }

            written += print_to_file(f, ")");

            return written;
        }
        default:
            create_generic_error("A Lisp_Object of type-id %d cannot be converted to a string",
                                 (u8)(node->type));
            return 0;
        }
    }

    proc print_lisp_object(FILE* file, Lisp_Object* node) -> int {
        return print_lisp_object_optional(file, node, false);
    }

    proc print_lisp_object_repr(FILE* file, Lisp_Object* node) -> int {
        return print_lisp_object_optional(file, node, true);
    }

    proc print_lisp_object_type(FILE* file, Lisp_Object_Type type) -> int {
        switch (type) {
        case(Lisp_Object_Type::Nil):          return print_to_file(file, "nil");
        case(Lisp_Object_Type::T):            return print_to_file(file, "t");
        case(Lisp_Object_Type::Number):       return print_to_file(file, "number");
        case(Lisp_Object_Type::String):       return print_to_file(file, "string");
        case(Lisp_Object_Type::Symbol):       return print_to_file(file, "symbol");
        case(Lisp_Object_Type::Keyword):      return print_to_file(file, "keyword");
        case(Lisp_Object_Type::Function):     return print_to_file(file, "function");
        case(Lisp_Object_Type::Continuation): return print_to_file(file, "continuation");
        case(Lisp_Object_Type::Pair):         return print_to_file(file, "pair");
        case(Lisp_Object_Type::Vector):       return print_to_file(file, "vector");
        case(Lisp_Object_Type::Pointer):      return print_to_file(file, "pointer");
        case(Lisp_Object_Type::HashMap):      return print_to_file(file, "hashmap");
        case(Lisp_Object_Type::Invalid_Garbage_Collected):  return print_to_file(file, "Invalid: Garbage Collected");
        case(Lisp_Object_Type::Invalid_Under_Construction): return print_to_file(file, "Invalid: Under Construction");
        }
        return print_to_file(file, "unknown");
    }


    proc print_single_call(Lisp_Object* obj) -> void {
        print("%{cyan}%{l_o_r}%{normal}\n   at ", obj);
        // TODO(Felix): Enable again when we have a source code
        // location again

        // if (obj->sourceCodeLocation) {
        //     printf("%s (line %d, position %d)",
        //            Memory::get_c_str(
        //                obj->sourceCodeLocation->file),
        //            obj->sourceCodeLocation->line,
        //            obj->sourceCodeLocation->column);
        // } else {
            fputs("no source code location avaliable", stdout);
        // }
    }

    proc print_current_execution() -> void {
        using Globals::Current_Execution;

        printf("cs:\n ");
        for (u32 i = 0; i < Current_Execution.cs.next_index; ++i)  {
            print(" %d: %{l_o_r}\n ", i, Current_Execution.cs.data[i]);
        }
        printf("\npcs:\n ");
        for (auto lo : Current_Execution.pcs)  {
            print("%{l_o_r}\n",lo);
        }
        printf("\nnnas:\n ");
        for (auto nas: Current_Execution.nass) {
            printf("nas:\n ");
            for (auto na : nas)  {
                printf("  - %s\n ", [&]
                    {
                        switch(na) {
                        case NasAction::Macro_Write_Back: return "Macro_Write_Back";
                        case NasAction::And_Then_Action:  return "And_Then_Action";
                        case NasAction::Pop_Environment:  return "Pop_Environment";
                        case NasAction::Define_Var:       return "Define_Var";
                        case NasAction::Eval:             return "Eval";
                        case NasAction::Step:             return "Step";
                        case NasAction::TM:               return "TM";
                        case NasAction::Pop:              return "Pop";
                        case NasAction::If:               return "If";
                        }
                        return "??";
                    }());
            }
        }
        printf("\nams:\n ");
        for (auto am : Current_Execution.ams)  {
            printf("%d\n ", am);
        }
    }

    proc log_error() -> void {
        fputs("\n", stdout);
        fputs(console_red,    stdout);
        fputs(Globals::error->message, stdout);
        puts(console_normal);

        fputs("  in: ", stdout);
        print_current_execution();
        puts(console_normal);
    }
}
