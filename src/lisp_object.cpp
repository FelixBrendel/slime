namespace Slime {
    proc create_source_code_location(String file, u32 line, u32 col) -> Source_Code_Location* {
        if (!file.data)
            return nullptr;

        Source_Code_Location* ret = (Source_Code_Location*)malloc(sizeof(Source_Code_Location));
        ret->file   = file;
        ret->line   = line;
        ret->column = col;
        return ret;
    }

}
