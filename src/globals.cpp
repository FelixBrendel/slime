namespace Slime {
#define v_major 0
#define v_minor 2
#define STRINGIZE2(s) #s
#define STRINGIZE(s) STRINGIZE2(s)
#define VERSION_STRING "v" STRINGIZE(v_major) "." STRINGIZE(v_minor) " - built on " __DATE__ " " __TIME__
    const char* version_string = VERSION_STRING;
    const u32 major_version = v_major;
    const u32 minor_version = v_minor;
#undef v_major
#undef v_minor
#undef STRINGIZE2
#undef STRINGIZE
#undef VERSION_STRING
}

namespace Slime::Globals {
    char* bin_path = nullptr;
    Log_Level log_level = Log_Level::Debug;
    bool debug_log = false;
    Array_List<path_char*> load_path;

    Hash_Map<void*, char*>                docs;
    Hash_Map<void*, Source_Code_Location> source_code_locations;
    Hash_Map<void*, Lisp_Object*>         user_types;

    Continuation Current_Execution;

    Error* error = nullptr;
#ifdef _DONT_BREAK_ON_ERRORS
    bool breaking_on_errors = false;
#else
    bool breaking_on_errors = true;
#endif
}
