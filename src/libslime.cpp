#define _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_DEPRECATE

#include <stdlib.h>

#include <stdio.h>
#include <time.h>
#include <string.h>
#include <cmath>
#include <ctype.h>
#include <stdarg.h>
#include <errno.h>
#include <new>


#if defined(_MSC_VER) || defined(_WIN32) || defined(WIN32) || defined(_WIN64)
#  include <direct.h>
#  include <windows.h>
#else
#  include <limits.h>
#  include <unistd.h>
#  include <signal.h>
#endif

#include "ftb/types.hpp"
/* NOTE(Felix): Forward declare the hash functions for the hashmap
  (needed at least for clang++)
*/
namespace Slime {struct Lisp_Object;}
bool hm_objects_match(char* a, char* b);
bool hm_objects_match(void* a, void* b);
bool hm_objects_match(Slime::Lisp_Object* a, Slime::Lisp_Object* b);
u32 hm_hash(char* str);
u32 hm_hash(void* ptr);
u32 hm_hash(Slime::Lisp_Object* obj);
#include "ftb/hashmap.hpp"
#include "ftb/arraylist.hpp"
#include "ftb/bucket_allocator.hpp"
#include "ftb/macros.hpp"
#include "ftb/profiler.hpp"
#include "ftb/hooks.hpp"
#include "ftb/print.hpp"

#    include "defines.cpp"
#    include "assert.hpp"
#    include "define_macros.hpp"
#    include "platform.cpp"
#    include "structs.cpp"
#    include "forward_decls.cpp"

inline bool hm_objects_match(Slime::Lisp_Object* a, Slime::Lisp_Object* b) {
    return Slime::lisp_object_equal(a, b);
}

u32 hm_hash(Slime::Lisp_Object* obj) {
    using namespace Slime;
    switch (obj->type) {
        // hash from adress: if two objects of these types have
        // different addresses, they are different
    case Lisp_Object_Type::Function:
    case Lisp_Object_Type::Symbol:
    case Lisp_Object_Type::Keyword:
    case Lisp_Object_Type::Continuation:
    case Lisp_Object_Type::Nil:
    case Lisp_Object_Type::T:
        return hm_hash((void*) obj);
        // hash from contents: even if objects are themselved
        // different, they cauld be equivalent:
    case Lisp_Object_Type::Pointer: return hm_hash((void*) obj->value.pointer);
    case Lisp_Object_Type::Number:  return hm_hash((void*) (unsigned long long)obj->value.number); // HACK(Felix): yes
    case Lisp_Object_Type::String:  return hm_hash((char*) obj->value.string.data);
    case Lisp_Object_Type::Pair: {
        u32 hash = 1;
        for_lisp_list (obj) {
            hash <<= 1;
            hash += hm_hash(it);
        }
        return hash;
    } break;
    case Lisp_Object_Type::Vector:
    case Lisp_Object_Type::HashMap:
    default:
        create_not_yet_implemented_error();
        return 0;
    }
}

#    include "globals.cpp"
#    include "memory.cpp"
#    include "gc.cpp"
#    include "lisp_object.cpp"
#    include "error.cpp"
#    include "io.cpp"
#    include "env.cpp"
#    include "parse.cpp"
#    include "eval.cpp"
#    include "visualization.cpp"
#    include "docgeneration.cpp"
#    include "built_ins.cpp"
#    include "testing.cpp"
// #    include "undefines.cpp"
