#undef new
#undef proc

#undef if_debug
#undef assert
#undef concat_
#undef label

#undef try
#undef try_void

#undef define_array_list

#undef console_normal
#undef console_red
#undef console_green
#undef console_cyan
