namespace Slime::GC {
    proc maybe_mark(Environment* env) -> void;

    int current_mark;

    Array_List<Lisp_Object*> marked_objects;
    Array_List<String>       marked_strings;
    Array_List<Environment*> marked_environments;
    Array_List<Environment*> protected_environments;

    proc marked(Lisp_Object* node) -> bool {
        return false;
    }

    proc marked(Environment* env) -> bool {
        return false;
    }

    proc maybe_mark(Lisp_Object* node) -> void {
        if (marked(node))
            return;

        // mark object itself
        marked_objects.append(node);

        // mark docstring
        // TODO(Felix):
        // if (node->docstring)
            // marked_strings.append(node->docstring);

        // mark type specific data
        switch (node->type) {
        case Lisp_Object_Type::Pair: {
            for_lisp_list (node) {
                maybe_mark(it);
            }
        } break;
        case Lisp_Object_Type::Vector: {
            for_lisp_vector (node) {
                maybe_mark(it);
            }
        } break;
        case Lisp_Object_Type::String: {
            marked_strings.append(node->value.string);
        } break;
        case Lisp_Object_Type::Function: {
            // NOTE(Felix): We dont have to mark the symbols, keywords
            // for parameter names, as symbols and keywords are never
            // garbage collected
            maybe_mark(node->value.function->parent_environment);
            if (!node->value.function->is_c) {
                maybe_mark(node->value.function->body.lisp_body);
            }
            // mark the default arguemnt values:
            for (auto it : node->value.function->args.keyword.values) {
                if (it)
                    maybe_mark(it);
            }
        } break;
        default: break;
        }

    }

    proc maybe_mark(Environment* env) -> void {
        if (marked(env))
            return;

        marked_environments.append(env);

        for (auto p : env->parents) {
            maybe_mark(p);
        }

        // Lisp_Object* it = env->values[0];
        // for (int i = 0; i < env->next_index; it = env->values[++i]) {
        //     maybe_mark(it);
        // }
    }

    proc garbage_collect() -> void {
        using Globals::Current_Execution;
        profile_this();
        ++current_mark;

        for (auto it : protected_environments)   maybe_mark(it);
        for (auto it : Current_Execution.envi_stack)               maybe_mark(it);
    }

    proc gc_init_and_go() -> void {
        current_mark = 0;

        while (1) {
            garbage_collect();
        }
    }
}
