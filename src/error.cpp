namespace Slime {

    proc delete_error() -> void {
        using Globals::error;

        free(error);
        error = nullptr;
    }

    proc create_error(const char* c_func_name, const char* c_file_name,
                      u32 c_file_line, Lisp_Object* type, char* message) -> void
    {
        using Globals::error;
        delete_error();
        error = (Error*)malloc(sizeof(Error)) ;
        error->type = type;
        error->message = message;

        log_error();

        if (Globals::breaking_on_errors) {
            debug_break();
        }
        if (Globals::log_level > Log_Level::None) {
            // c error location
            printf("in");
            s32 spacing = 30-((s32)strlen(c_file_name) + (s32)log10(c_file_line));
            if (spacing < 1) spacing = 1;
            for (s32 i = 0; i < spacing; ++i)
                printf(" ");
            printf("%s (%u) ", c_file_name, c_file_line);
            printf("-> %s\n", c_func_name);
        }

        // visualize_lisp_machine();
    }

    proc create_error(const char* c_func_name, const char* c_file_name,
                      u32 c_file_line, Lisp_Object* type, const char* format, ...) -> void {
        using Globals::error;
        if (error) {
            error = new(Error);
            error->type = type;
        }

        char* msg;

        va_list args;
        va_start(args, format);
        print_va_args_to_string(&msg, format, &args);
        va_end(args);

        create_error(c_func_name, c_file_name, c_file_line, type, msg);
    }
}
