
namespace Slime {

    proc create_extended_environment_for_function_application_nrc(Lisp_Object* function,
                                                                  u32 arg_start,
                                                                  u32 arg_end) -> Environment*
    {
        profile_this();
        using Globals::Current_Execution;

        u32 index_of_next_arg = arg_start;
        bool is_c_function = function->value.function->is_c;
        Environment* env = Memory::create_child_environment(function->value.function->parent_environment);
        Arguments* arg_spec = &function->value.function->args;

        Array_List<Lisp_Object*> read_in_keywords;
        read_in_keywords.alloc();
        defer {
            read_in_keywords.dealloc();
        };
        u32 obligatory_keywords_count = 0;
        u32 read_obligatory_keywords_count = 0;
        Lisp_Object* sym;
        Lisp_Object* val;
        // read positionals
        for (u32 i = 0; i < arg_spec->positional.symbols.next_index; ++i) {
            if (index_of_next_arg == arg_end) {
                create_parsing_error("Not enough positional args supplied. Needed: %d suppied, %d.\n"
                                     "Next missing arg is '%s'",
                                     arg_spec->positional.symbols.next_index, arg_end-index_of_next_arg,
                                     arg_spec->positional.symbols.data[i]->value.symbol.data);
                return nullptr;
            }
            // NOTE(Felix): We have to copy all the arguments,
            // otherwise we change the program code. To C functions we
            // pass by reference for better performance and trust them
            // to not mutate the arguments because we expect c
            // programmers to know what they are doing. Bold claim I
            // know.
            if (is_c_function) {
                define_symbol(arg_spec->positional.symbols.data[i], Current_Execution.cs.data[index_of_next_arg], env);
            } else {
                define_symbol(arg_spec->positional.symbols.data[i], Memory::copy_lisp_object_except_pairs(Current_Execution.cs.data[index_of_next_arg]), env);
            }
            ++index_of_next_arg;
        }

        // if there are some left read keywords and rest
        if (index_of_next_arg != arg_end) {
            // find out how many keyword args we /have/ to read
            for (u32 i = 0; i < arg_spec->keyword.values.next_index; ++i) {
                if (arg_spec->keyword.values.data[i] == nullptr)
                    ++obligatory_keywords_count;
            }

            while (Current_Execution.cs.data[index_of_next_arg]->type == Lisp_Object_Type::Keyword) {
                // check if this one is even an accepted keyword
                bool accepted = false;
                for (u32 i = 0; i < arg_spec->keyword.keywords.next_index; ++i) {
                    if (Current_Execution.cs.data[index_of_next_arg] == arg_spec->keyword.keywords.data[i]) {
                        accepted = true;
                        break;
                    }
                }
                if (!accepted) {
                    // if we read all we need then we are done here
                    if (read_obligatory_keywords_count == obligatory_keywords_count)
                        break;
                    // otherwise we would have to read more but there
                    // was a not accepted kwarg, so signal the error
                    create_generic_error(
                                         "The function does not take the keyword argument ':%s'\n"
                                         "and not all required keyword arguments have been read\n"
                                         "in to potentially count it as the rest argument.",
                                         Current_Execution.cs.data[index_of_next_arg]->value.symbol.data);
                    return nullptr;
                }
                // This is an accepted kwarg; check if it was already
                // read in
                for (u32 i = 0; i < read_in_keywords.next_index; ++i) {
                    if (Current_Execution.cs.data[index_of_next_arg] == read_in_keywords.data[i])
                    {
                        // if we already read it in but also finished
                        // all other kwargs, then count it as rest and
                        // be done here
                        if (read_obligatory_keywords_count == obligatory_keywords_count)
                            goto kw_done;
                        // If there are some kwargs left to be read
                        // in, it is an error
                        create_generic_error(
                                             "The function already read the keyword argument ':%s'",
                                             Current_Execution.cs.data[index_of_next_arg]->value.symbol.data);
                        return nullptr;
                    }
                }
                // okay so we found a keyword that has to be read in and was
                // not already read in, is there a next element to actually
                // set it to?
                if (index_of_next_arg+1 == arg_end) {
                    create_generic_error(
                                         "Attempting to set the keyword argument ':%s', but no value was supplied.",
                                         Current_Execution.cs.data[index_of_next_arg]->value.symbol.data);
                    return nullptr;
                }

                // if not set it and then add it to the array list
                Lisp_Object* key = Current_Execution.cs.data[index_of_next_arg];
                try sym = Memory::get_symbol(key->value.symbol);
                ++index_of_next_arg;

                if (is_c_function) {
                    try define_symbol(sym, Current_Execution.cs.data[index_of_next_arg], env);
                } else {
                    try define_symbol(sym, Memory::copy_lisp_object_except_pairs(Current_Execution.cs.data[index_of_next_arg]), env);
                }

                read_in_keywords.append(key);
                ++read_obligatory_keywords_count;

                ++index_of_next_arg;

                if (index_of_next_arg == arg_end) {
                    break;
                }
            }
        }


        /*c
plot_title('Sine Wave')
plot_function_samples(1000)
plot_xaxis(0,30)
plot_yaxis(-1.5,1.5)
plot(sin(t))
*/
        kw_done:
        // check keywords for completeness
        for (u32 i = 0; i < arg_spec->keyword.values.next_index; ++i) {
            auto defined_keyword = arg_spec->keyword.keywords.data[i];
            bool was_set = false;
            for (u32 j = 0; j < read_in_keywords.next_index; ++j) {
                if (read_in_keywords.data[j] == defined_keyword) {
                    was_set = true;
                    break;
                }
            }
            if (arg_spec->keyword.values.data[i] == nullptr) {
                // if this one does not have a default value
                if (!was_set) {
                    create_generic_error(
                                         "There was no value supplied for the required "
                                         "keyword argument ':%s'.",
                                         defined_keyword->value.symbol.data);
                    return nullptr;
                }
            } else {
                // this one does have a default value, lets see if we have
                // to use it or if the user supplied his own
                if (!was_set) {
                    try sym = Memory::get_symbol(defined_keyword->value.symbol);
                    if (is_c_function) {
                        try val = arg_spec->keyword.values.data[i];
                    } else {
                        try val = Memory::copy_lisp_object_except_pairs(arg_spec->keyword.values.data[i]);
                    }
                    define_symbol(sym, val, env);
                }
            }
        }

        // read in rest arg
        if (index_of_next_arg == arg_end) {
            if (arg_spec->rest) {
                define_symbol(arg_spec->rest, Memory::nil, env);
            }
        } else {
            if (arg_spec->rest) {
                Lisp_Object* list;
                try list = Memory::create_list(Current_Execution.cs.data[index_of_next_arg]);
                Lisp_Object* head = list;
                for (++index_of_next_arg;index_of_next_arg < arg_end; ++index_of_next_arg) {
                    try head->value.pair.rest = Memory::create_list(Current_Execution.cs.data[index_of_next_arg]);
                    head = head->value.pair.rest;
                }
                define_symbol(arg_spec->rest, list, env);
            } else {
                // rest was not declared but additional arguments were found
                create_generic_error("A rest argument was not declared "
                                     "but the function was called with additional arguments.");
                return nullptr;
            }
        }

        return env;
    }

    proc create_arguments_from_lambda_list_and_inject(Lisp_Object* arguments, Lisp_Object* function) -> void {
        /* NOTE This parses the argument specification of funcitons
         * into their Function struct. It does this by allocating new
         * positional_arguments, keyword_arguments and rest_argument
         * and filling it in
         */
        Arguments* result = &function->value.function->args;;

        // first init the fields
        result->rest       = nullptr;

        // okay let's try to read some positional arguments
        while (arguments->type == Lisp_Object_Type::Pair) {
            // if we encounter a keyword or a list (for keywords with
            // defualt args), the positionals are done
            if (arguments->value.pair.first->type == Lisp_Object_Type::Keyword ||
                arguments->value.pair.first->type == Lisp_Object_Type::Pair) {
                break;
            }

            // if we encounter something that is neither a symbol nor a
            // keyword arg, it's an error
            if (arguments->value.pair.first->type != Lisp_Object_Type::Symbol) {
                create_parsing_error("Only symbols and keywords "
                                     "(with or without default args) "
                                     "can be parsed here, but found '%{l_o_t}'",
                                     arguments->value.pair.first->type);
                return;
            }

            // okay we found an actual symbol
            result->positional.symbols.append(arguments->value.pair.first);

            arguments = arguments->value.pair.rest;
        }

        // if we reach here, we are on a keyword or a pair wher a keyword
        // should be in first
        while ((arguments->type) == Lisp_Object_Type::Pair) {
            if ((arguments->value.pair.first->type) == Lisp_Object_Type::Keyword) {
                // if we are on a actual keyword (with no default arg)
                auto keyword = arguments->value.pair.first;
                result->keyword.keywords.append(keyword);
                result->keyword.values.append(nullptr);
            } else if ((arguments->value.pair.first->type) == Lisp_Object_Type::Pair) {
                // if we are on a keyword with a default value

                auto keyword = arguments->value.pair.first->value.pair.first;
                if ((keyword->type) != Lisp_Object_Type::Keyword) {
                    create_parsing_error("Default args must be keywords");
                }
                if ((arguments->value.pair.first->value.pair.rest->type)
                    != Lisp_Object_Type::Pair)
                {
                    create_parsing_error("Default args must be a list of 2.");
                }
                auto value = arguments->value.pair.first->value.pair.rest->value.pair.first;
                try_void value = eval_expr(value);
                if (arguments->value.pair.first->value.pair.rest->value.pair.rest != Memory::nil) {
                    create_parsing_error("Default args must be a list of 2.");
                }

                result->keyword.keywords.append(keyword);
                result->keyword.values.append(value);
            }
            arguments = arguments->value.pair.rest;
        }

        // Now we are also done with keyword arguments, lets check for
        // if there is a rest argument
        if (arguments->type != Lisp_Object_Type::Pair) {
            if (arguments == Memory::nil)
                return;
            if (arguments->type == Lisp_Object_Type::Symbol)
                result->rest = arguments;
            else
                create_parsing_error("The rest argument must be a symbol.");
        }
    }


    proc list_length(Lisp_Object* node) -> u32 {
        if (node == Memory::nil)
            return 0;

        try assert_type(node, Lisp_Object_Type::Pair);

        u32 len = 0;

        while (node->type == Lisp_Object_Type::Pair) {
            ++len;
            node = node->value.pair.rest;
            if (node == Memory::nil)
                return len;
        }

        create_parsing_error("Can't calculate length of ill formed list.");
        return 0;
    }

    proc copy_scl(Source_Code_Location*) -> Source_Code_Location* {
        // TODO(Felix):
        return nullptr;
    }

    proc pause() {
        printf("\n-----------------------\n"
               "Press ENTER to continue\n");
        getchar();
    }

    inline proc maybe_wrap_body_in_begin(Lisp_Object* body) -> Lisp_Object* {
        Lisp_Object* begin_symbol = Memory::get_symbol("begin");
        if (body->type != Lisp_Object_Type::Pair)
            return body;
        if (body->value.pair.rest == Memory::nil)
            return body->value.pair.first;
        else
            return Memory::create_lisp_object_pair(begin_symbol, body);
    }

    proc eval_expr(Lisp_Object* expr) -> Lisp_Object* {
        profile_this();
        using Globals::Current_Execution;

        Current_Execution.nass.reserve(1);
        Array_List<NasAction>* nas = Current_Execution.nass.data+(Current_Execution.nass.next_index++);
        nas->alloc();
        defer {
            --Current_Execution.nass.next_index;
            nas->dealloc();
        };

        proc debug_step = [&] {
            if (!Globals::debug_log)
                return;
            printf("\n-------------------\n");
            print_current_execution();
            // pause();
        };

        proc push_pc_on_cs = [&] {
            for_lisp_list (Current_Execution.pcs.data[Current_Execution.pcs.next_index-1]) {
                Current_Execution.cs.append(it);
            }
            Current_Execution.pcs.data[Current_Execution.pcs.next_index-1] = Memory::nil;
        };

        Current_Execution.cs.append(expr);
        nas->append(NasAction::Eval);

        NasAction current_action;
        Lisp_Object* pc;

        while (nas->next_index > 0) {
            debug_step();

            current_action = nas->data[--nas->next_index];
            switch (current_action) {
                case NasAction::Pop: {
                    --Current_Execution.cs.next_index;
                } break;
                case NasAction::And_Then_Action: {
                    Current_Execution.ats.data[--Current_Execution.ats.next_index]();
                } break;
                case NasAction::Pop_Environment: {
                    pop_environment();
                } break;
                case NasAction::Eval: {
                    pc = Current_Execution.cs.data[Current_Execution.cs.next_index-1];
                    Lisp_Object_Type type = pc->type;
                    switch (type) {
                        case Lisp_Object_Type::Symbol: {
                            try Current_Execution.cs.data[Current_Execution.cs.next_index-1]
                                = lookup_symbol(pc, get_current_environment());
                        } break;
                        case Lisp_Object_Type::Pair: {
                            Current_Execution.cs.data[Current_Execution.cs.next_index-1] = pc->value.pair.first;
                            Current_Execution.ams.append(Current_Execution.cs.next_index-1);

                            if_debug {
                                if (Current_Execution.ams.next_index >= 2) {
                                    assert("invalid ams state",
                                           Current_Execution.ams.data[Current_Execution.ams.next_index-2] <=
                                           Current_Execution.ams.data[Current_Execution.ams.next_index-1]);
                                }
                            }

                            Current_Execution.pcs.append(pc->value.pair.rest);
                            Current_Execution.mes.append(pc);
                            nas->append(NasAction::TM);
                            nas->append(NasAction::Eval);
                        } break;
                        default: {
                            // NOTE(Felix): others are self evaluating
                            // so do nothing
                        }
                    }
                } break;
                case NasAction::Macro_Write_Back: {
                    *(Current_Execution.mes.data[--Current_Execution.mes.next_index])
                        = *Current_Execution.cs[Current_Execution.cs.next_index-1];
                } break;
                case NasAction::TM: {
                    pc = Current_Execution.cs.data[Current_Execution.cs.next_index-1];

                    Lisp_Object_Type type = pc->type;
                    switch (type) {
                        case Lisp_Object_Type::Function: {
                            if(pc->value.function->is_c) {
                                if (pc->value.function->type.c_function_type == C_Function_Type::cMacro) {
                                    --Current_Execution.cs.next_index;  // remove the macro call from cs
                                    --Current_Execution.ams.next_index; // remove the apply marker for the macro
                                    try pc->value.function->body.c_macro_body();
                                } else if (pc->value.function->type.c_function_type == C_Function_Type::cSpecial)
                                {
                                    // QUESTION(Felix): Why not call the
                                    // function right away, and instead push
                                    // step, so that step calls it?
                                    push_pc_on_cs();
                                    nas->append(NasAction::Step);
                                } else {
                                    nas->append(NasAction::Step);
                                }
                                --Current_Execution.mes.next_index;
                            } else {
                                if (pc->value.function->type.lisp_function_type ==
                                    Lisp_Function_Type::Macro)
                                {
                                    push_pc_on_cs();
                                    nas->append(NasAction::Eval);
                                    nas->append(NasAction::Macro_Write_Back);
                                    nas->append(NasAction::Step);
                                } else {
                                    --Current_Execution.mes.next_index;
                                    nas->append(NasAction::Step);
                                }
                            }
                        } break;
                        case Lisp_Object_Type::Continuation: {
                            --Current_Execution.mes.next_index;
                            --Current_Execution.ams.next_index;
                            Lisp_Object* param = Current_Execution.pcs.data[--Current_Execution.pcs.next_index];
                            try assert_list_length(param, 1);
                            param = param->value.pair.first;
                            // NOTE(Felix): we could first get value and eval
                            // it and restore the cont on an and_then_action
                            // OR we could restore the cont now and push the
                            // new unevaluated val on the stack and leave a
                            // NAS_Actoin::Eval behind. So that's what we
                            // gonna do.

                            Globals::Current_Execution.cs.clear();
                            Globals::Current_Execution.ams.clear();
                            Globals::Current_Execution.pcs.clear();
                            Globals::Current_Execution.nass.clear();
                            Globals::Current_Execution.envi_stack.clear();
                            Globals::Current_Execution.ats.clear();
                            Globals::Current_Execution.mes.clear();

                            // TODO(Felix): This seems super inefficient
                            for (auto it: pc->value.continuation->cs) {
                                Globals::Current_Execution.cs.append(it);
                            }
                            for (auto it: pc->value.continuation->ams) {
                                Globals::Current_Execution.ams.append(it);
                            }
                            for (auto it: pc->value.continuation->pcs) {
                                Globals::Current_Execution.pcs.append(it);
                            }
                            for (auto it: pc->value.continuation->envi_stack) {
                                Globals::Current_Execution.envi_stack.append(it);
                            }
                            for (auto it: pc->value.continuation->ats) {
                                Globals::Current_Execution.ats.append(it);
                            }
                            for (auto it: pc->value.continuation->mes) {
                                Globals::Current_Execution.mes.append(it);
                            }
                            {
                                Globals::Current_Execution.nass.reserve(pc->value.continuation->nass.next_index);
                                Globals::Current_Execution.nass.next_index = pc->value.continuation->nass.next_index;

                                for (u32 i = 0; i < pc->value.continuation->nass.next_index; ++i) {
                                    Globals::Current_Execution.nass.data[i].alloc();
                                    for (Globals::Current_Execution.nass.data[i].next_index = 0;
                                         Globals::Current_Execution.nass.data[i].next_index < pc->value.continuation->nass.data[i].next_index;)
                                    {
                                        Globals::Current_Execution.nass.data[i].append(
                                                                                       pc->value.continuation->nass.data[i].data[Globals::Current_Execution.nass.data[i].next_index]);
                                    }
                                }
                            }

                            Globals::Current_Execution.cs.append(param);
                            (Current_Execution.nass.end()-1)->append(NasAction::Eval);
                            // debug_break();
                        } break;
                        default: {
                            create_generic_error("The first element of the pair was not a function but: %{l_o_t} in %{l_o}",
                                                 type, pc);
                            return nullptr;
                        }
                    }

                } break;
                case NasAction::Step: {
                    if (Current_Execution.pcs.data[Current_Execution.pcs.next_index-1] == Memory::nil) {
                        --Current_Execution.pcs.next_index;
                        u32 am = Current_Execution.ams.data[--Current_Execution.ams.next_index];
                        Lisp_Object* function = Current_Execution.cs.data[am];
                        try assert_type(function, Lisp_Object_Type::Function);

                        Environment* extended_env;
                        try extended_env = create_extended_environment_for_function_application_nrc(
                                                                                                    function, am+1, Current_Execution.cs.next_index);
                        Current_Execution.cs.next_index = am;
                        push_environment(extended_env);
                        if (function->value.function->is_c) {
                            if (function->value.function->type.c_function_type == C_Function_Type::cMacro)
                                try function->value.function->body.c_macro_body();
                            else
                                try Current_Execution.cs.append(function->value.function->body.c_body());
                            pop_environment();
                        } else {
                            nas->append(NasAction::Pop_Environment);
                            nas->append(NasAction::Eval);
                            Current_Execution.cs.append(function->value.function->body.lisp_body);
                        }
                    } else {
                        Current_Execution.cs.append(Current_Execution.pcs.data[Current_Execution.pcs.next_index-1]->value.pair.first);
                        Current_Execution.pcs.data[Current_Execution.pcs.next_index-1] = Current_Execution.pcs.data[Current_Execution.pcs.next_index-1]->value.pair.rest;
                        nas->append(NasAction::Step);
                        nas->append(NasAction::Eval);
                    }
                } break;
                case NasAction::If: {
                    /* |  <cond>  |
                       |  <then>  |
                       |  <else>  |
                       |   ....   | */
                    Current_Execution.cs.next_index -= 2;
                    // NOTE(Felix): for false it is sufficent to pop 2 for
                    // true we have to copy the then part to the new top
                    // of the stack
                    if (Current_Execution.cs.data[Current_Execution.cs.next_index+1] != Memory::nil) {
                        Current_Execution.cs.data[Current_Execution.cs.next_index-1] = Current_Execution.cs.data[Current_Execution.cs.next_index];
                    }
                } break;
                case NasAction::Define_Var: {
                    /* |  <thing>   |
                       |  <symbol>  |
                       |    ....    | */
                    Current_Execution.cs.next_index -= 1;
                    try assert_type(Current_Execution.cs.data[Current_Execution.cs.next_index-1], Lisp_Object_Type::Symbol);
                    try define_symbol(Current_Execution.cs.data[Current_Execution.cs.next_index-1], Current_Execution.cs.data[Current_Execution.cs.next_index]);
                    Current_Execution.cs.data[Current_Execution.cs.next_index-1] = Memory::t;
                }
            }

        }
        // debug_step();

        return Current_Execution.cs.data[--Current_Execution.cs.next_index];
    }

    inline proc is_truthy(Lisp_Object* expression) -> bool {
        return expression != Memory::nil;
    }

    proc interprete_file (char* file_name) -> Lisp_Object* {
        Lisp_Object* result;

        try result = built_in_load(Memory::create_string(file_name));

        return result;
    }

    proc interprete_stdin() -> void {
        printf("Welcome to the lispy interpreter.\n%s\n", version_string);

        char* line;

        Lisp_Object* parsed, * evaluated;
        while (true) {
            if (Globals::error) {
                log_error();
                delete_error();
            }
            fputs("> ", stdout);
            line = read_expression();
            parsed = Parser::parse_single_expression(line);
            if (Globals::error) {
                continue;
            }
            free(line);
            evaluated = eval_expr(parsed);
            if (Globals::error) {
                continue;
            }
            if (evaluated && evaluated != Memory::nil) {
                print("%{l_o}", evaluated);
            }
            fputs("\n", stdout);
        }
    }
}
