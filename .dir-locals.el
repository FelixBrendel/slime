((nil . ((eval . (progn
                   (defvar context-mode-map (make-sparse-keymap)
                     "Keymap while context-mode is active.")
                   (define-minor-mode context-mode
                     "A temporary minor mode to be activated only specific to a buffer."
                     nil
                     :lighter " [f2]-Context"
                     context-mode-map)
                   (context-mode 1)

                   (defun start-debugger ()
                     (interactive)
                     (let ((default-directory (expand-windows-path (concat (projectile-project-root) "bin/"))))
                       (unless (process-running-p "remedybg.exe")
                         (start-process "remedy" "*remedyout*"
                                        "remedybg.exe" "open-session"
                                        (expand-windows-path (concat (projectile-project-root) "bin/slime.rdbg"))))

                       (start-process "remedy" "*remedyout*" "remedybg.exe" "start-debugging")))

                   (defhydra hydra-context (context-mode-map "<f2>")
                     "Context Actions:"
                     ("b" save-and-find-build-script-and-compile "build" :color blue)
                     ("d" start-debugger                         "debug" :color blue)
                     ("o" browse-file-directory                  "open"  :color blue)
                     ("q" nil                                    "quit"  :color blue))

                   (define-key context-mode-map (kbd "<f2>") 'hydra-context/body)

                   (font-lock-add-keywords
                    'c++-mode
                    '(("\\<\\(if_debug\\|if_unicode\\|if_windows\\|if_linux\\|defer\\|proc\\|try\\|try_void\\|for_array_list\\|for_hash_map\\|for_lisp_list\\|for_lisp_vector\\|in_caller_env\\|ignore_logging\\|dont_break_on_errors\\)\\>" .
                       font-lock-keyword-face)))))))

 (c++-mode . ((eval . (company-clang-set-prefix "slime.h"))
              (eval . (flycheck-mode 0))
              (eval . (company-mode 0))
              (eval . (rainbow-mode 0))
              (eval . (setq c-backslash-max-column 99)))))
