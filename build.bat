@echo off
@setlocal
pushd %~dp0\bin

set exeName=slime.exe

taskkill /F /IM %exeName% > NUL 2> NUL

echo ---------- Compiling ----------
call ..\timecmd cl  ^
     ../src/main.cpp^
     /I../3rd/ ^
     /D_PROFILING /D_DEBUG /D_DONT_BREAK_ON_ERRORS ^
     /Zi /std:c++latest /Fe%exeName% /W3 /wd4003 /wd4996 /nologo /EHsc


rem call ..\timecmd cl  ^
     rem ../src/main.cpp^
     rem /I../3rd/ ^
     rem /O2 /D_DONT_BREAK_ON_ERRORS ^
     rem /std:c++latest /Fe%exeName% /W3 /wd4003 /nologo /EHsc

rem call ..\timecmd clang-cl ../src/main.cpp /I../3rd/ -o %exeName% /O2 /std:c++latest /W3 /Zi /EHsc

if %errorlevel% == 0 (
   echo.
   echo -------- Running Tests --------
   echo.
   call ..\timecmd slime.exe --run-tests
   echo.
   echo -------- Generatign Docs --------
   echo.
   call ..\timecmd slime.exe --generate-docs-file
   rem call ..\manual\build.bat
) else (
  echo.
  echo Fuckin' ell
)

popd
