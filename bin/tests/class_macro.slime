(import "oo.slime")

(define-class (vector3 x y z)
  (define (set-x new-x) (mutate! x new-x))
  (define (set-y new-y) (mutate! y new-y))
  (define (set-z new-z) (mutate! z new-z))

  (define (length)
    (** (+ (* x x) (* y y) (* z z)) 0.5))

  (define (scale fac)
    (mutate! x (* fac x))
    (mutate! y (* fac y))
    (mutate! z (* fac z))
    fac)

  (define (add other)
    (make-vector3
     (+ (-> other x) x)
     (+ (-> other y) y)
     (+ (-> other z) z)))

  (define (subtract other)
    (make-vector3
     (- (-> other x) x)
     (- (-> other y) y)
     (- (-> other z) z)))

  (define (equal? other)
    (and (= (-> other x) x)
         (= (-> other y) y)
         (= (-> other z) z)))

  (define (scalar-product other)
    (+ (* (-> other x) x)
       (* (-> other y) y)
       (* (-> other z) z)))

  (define (cross-product other)
    (make-vector3
     (- (* (-> other z) y) (* (-> other y) z))
     (- (* (-> other x) z) (* (-> other z) x))
     (- (* (-> other y) x) (* (-> other x) y))))

  (define (print)
    (printf :sep " " "[vector3] (" x  y  z ")"))
  )

(define v1 (make-vector3 1 2 3))
(define v2 (make-vector3 3 2 1))

(assert (= (type v1) (type v2) :vector3))
(assert (= (v1 'scalar-product v2) 10))
(assert (-> (-> v1 cross-product v2)
            equal?
            (make-vector3 -4 8 -4)))
