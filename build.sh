TIMEFORMAT=%3lU
SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"
pushd $SCRIPTPATH > /dev/null

# echo ""
# echo "----------------------"
# echo "  compiling libslime  "
# echo "----------------------"
# time clang++ --std=c++17 \
#         src/libslime.cpp -c -o libslime.o \
#         -I3rd/ || exit 1

echo ""
echo "------------------------------"
echo " compiling fullslime  (debug) "
echo "------------------------------"
time clang++ -D_DEBUG -D_DONT_BREAK_ON_ERRORS \
     src/main.cpp -gfull -gdwarf -o ./bin/slime_d --std=c++17 \
     -I3rd/ || exit 1

echo ""
echo "--------------------------------"
echo " compiling fullslime  (release) "
echo "--------------------------------"
time clang++ -D_DONT_BREAK_ON_ERRORS -O3  \
     src/main.cpp -g -o ./bin/slime --std=c++17 \
     -I3rd/ || exit 1

echo ""
echo "------------------------------"
echo " compiling fullslime  (prof)  "
echo "------------------------------"
time clang++ -D_DONT_BREAK_ON_ERRORS -D_PROFILING \
     src/main.cpp -o ./bin/slime_p --std=c++17 \
     -I3rd/ || exit 1

pushd ./bin > /dev/null

# echo ""
# echo "----------------------"
# echo "   generating docs    "
# echo "----------------------"
# time valgrind -q ./slime_d --generate-docs-file || exit 1

echo ""
echo "----------------------"
echo "    running tests     "
echo "----------------------"
time valgrind -q --track-origins=yes --leak-check=full --show-leak-kinds=all ./slime_d --run-tests || exit 1

echo ""
echo "----------------------"
echo "    running profile   "
echo "----------------------"
time ./slime_p --run-tests > /dev/null || exit 1

echo ""
echo "------------------------"
echo "    running benches     "
echo "------------------------"
hyperfine -s color --warmup 5 "./slime --run-tests > /dev/null"

popd > /dev/null
# popd > /dev/null
unset TIMEFORMAT
