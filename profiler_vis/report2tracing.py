#!/usr/bin/python

import json
import csv
import sys

class FancyFloat(float):
    def __repr__(self):
        return format(Decimal(self), "f")

class JsonRpcEncoder(json.JSONEncoder):
    def decimalize(self, val):
        if isinstance(val, dict):
            return {k:self.decimalize(v) for k,v in val.items()}

        if isinstance(val, (list, tuple)):
            return type(val)(self.decimalize(v) for v in val)

        if isinstance(val, float):
            return FancyFloat(val)

        return val

    def encode(self, val):
        return super().encode(self.decimalize(val))

if len(sys.argv) == 1:
    print("No file was provided")
else:
    trace_events = []
    call_stack = []
    with open(sys.argv[1], "r") as in_file:
        csv_reader = csv.reader(in_file, delimiter=',')
        pf = 1
        first_line = True
        last_ts = -1;
        for line in csv_reader:
            if first_line:
                pf = float(line[0]) / 1000
                first_line = False
                continue
            if line[0] == "->":
                call_stack.append(line)
            elif line[0] == "<-":
                call = call_stack.pop()
                ts = float(call[1])
                dur = (float(line[1])-ts)
                dict = {
                    "pid": 1,
                    "tid": 1,
                    "ts" : ts,
                    "dur": dur,
                    "ph" : "X",
                    "name": call[2],
                    "args": {
                        "file":  f"({call[3]}:{call[4]})",
                    }
                }
                if call[5]:
                    dict["args"]["info1"] = call[5]
                if call[6]:
                    dict["args"]["info2"] = call[6]
                trace_events.append(dict)
            else:
                print("invalid syntax")
                break
    with open("out.json", "w") as out_file:
        out_file.write(json.dumps({"traceEvents": trace_events}, indent=4))
