echo ================================================
echo               Starting Tex Export
echo ================================================

FILENAME=manual

emacsclient -c --frame-parameters="((visibility . nil))" \
            -e "(progn (require 'org) (find-file-other-window \"$FILENAME.org\") (org-latex-export-to-latex) (save-buffers-kill-terminal))"  || exit 1


echo ================================================
echo               Tex Export Finished
echo ================================================


latexmk -Werror -pdf -shell-escape $FILENAME.tex || exit 1
latexmk -c $FILENAME.tex
